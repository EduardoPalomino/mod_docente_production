<!-- OBLIGATORIO : For Prisma generate follow this instruction -->
ON TERMINAL --> npm i

<!-- OBLIGATORIO : For Prisma generate follow this instruction -->
ON TERMINAL --> cd server/prisma
CODE --> npx prisma generate


<!-- OPCIONAL USAR EN CASO NO EXISTA TABLAS EN BD-->
<!-- ON TERMINAL: cd server/prisma
CODE: npx prisma migrate dev --name tablas -->

<!-- OPCIONAL -->
<!-- PARA ENVIO DE CORREO INGRESAR A
1. https://myaccount.google.com/security
2. Debe estar activado la configuracion en dos pasos
3. Seguridad -> Verificacion en 2 pasos -> Contraseña de aplicaciones
4. Escoger Otros y colocar de nombre isilgoapp
5. Remitir el codigo generado y colocar en variable de entorno MAIL_AUTH_APP_PASSWORD -->