import { auth } from "express-openid-connect";

const config = {
    authRequired: false,
    auth0Logout: true,
    secret: process.env.SECRET,
    baseURL: process.env.BASEURL_AUTH,
    clientID: process.env.CLIENT_ID,
    issuerBaseURL: process.env.ISSUERBASEURL
};


export const validateAuth = () => {
  return auth(config);
} 