export class DefaultResponse {
    Status: string;
    Message: string;
    Data: any[];
}