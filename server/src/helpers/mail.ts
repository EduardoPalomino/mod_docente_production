import nodemailer from "nodemailer";
import { MESSAGE } from "./constants";

var transporter = nodemailer.createTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: process.env.MAIL_AUTH_USER,
      pass: process.env.MAIL_AUTH_APP_PASSWORD, // APP PASSWORD
    }
  });

  const mailOptions  = {
    from: process.env.MAIL_AUTH_USER,
    to: '',
    subject: MESSAGE.RECOVER_PASSWORD,
    html: ``

  };

export const sendMail = async (endUser) =>{
    const htmlPart = `<h4>Ingrese al siguiente enlace para reetablecer su contraseña: </h4><p><a href=${process.env.LINK_RECOVER_PASSWORD}>ISIL!</a></p>`
    mailOptions.to = endUser;
    mailOptions.html = htmlPart;
    const send = {
        sendMailIsil: (options) => new Promise((resolve, reject) => {
            transporter.sendMail(options, function(error, info){
                if (error) {
                  reject(error)
                } else {
                  resolve('');
                }
              });
        }),
      };
    
      return send.sendMailIsil(mailOptions).catch(e => e.stack)
}