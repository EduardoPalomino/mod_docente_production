const TABLES = {
    TABLE_USER: 'usuario',
    TABLE_ROL: 'rol',
    TABLE_CATEGORY: 'categoria',
    TABLE_TEACHER: 'docente',
    TABLE_COURSE_TEACHER: 'docente_curso',
    TABLE_COURSE: 'curso',
    TABLE_COURSE_SELL: 'curso_vendido',
    TABLE_COMMISSION: 'comision'
}

const API = {
    //VALIDATION
    VALIDATION: '/',
    //USER
    VALIDATE_USER: '/api/validateInfo',
    ALL_USERS: '/api/getAllUsers',    
    POST_ALL_TEACHER_COURSE_STATE_COMMISSION_RATE: '/api/usuario/admin/AllDocenteEstadoCursoTazaComision', //E
    PUT_TEACHER_INFO: '/api/usuario/admin/UpdateDocenteInfo', //E
    GET_STATE_TEACHER: '/api/estado/listDocenteEstado', //E
    GET_LIST_ALL_RATE_TYPE: '/api/tipo-tasa/listAlltipoTasa', //E

    //CATEGORIES
    //ROLES
    //LOGIN
    //COURSE
    GET_COURSES_BY_TEACHER: '/api/cursos/listado',
    GET_LIST_ALL_COURSE: '/api/listAllCurso',
    //COMMISION
    GET_GENERAL_DETAIL: '/api/comision/detalleGeneral',
    GET_TOTAL_COMMISSION: '/api/comision/total',
    GET_TOTAL_COMMISSION_FILTER: '/api/comision/totalFilter',
    POST_ALL_USER_COMMISSION: '/api/comision/postAllComisionUser', //E
    POST_ALL_COURSE_COMMISSION: '/api/comision/postAllComisionCurso', //E
    UPDATE_COMMISSION_COURSE_MASSIVE: '/api/comision/updateComisionMasivoCurso', //E
    UPDATE_COMMISSION_TEACHER_MASSIVE: '/api/comision/updateComisionMasivoDocente',  //E
    //DOCENTE
    POST_ALL_TEACHER_COMMISSION_COURSE: '/api/listAllDocenteCursoComision',
    GET_LIST_ALL_TEACHER:'/api/docente/listAllDocente', //E
    GET_TEACHER_INFO: '/api/docente/info',
}

const SP = {
}

const STATUS = {
    OK: 'OK',
    FAIL: 'FAIL'
}

const MESSAGE = {
    LOGGED_IN: 'INICIO DE SESION EXITOSO',
    LOGGED_OUT: 'CERRAR SESION EXITOSO',
    PASSWORD_NOTFOUND: 'CONTRASEÑA INCORRECTA',
    USER_NOTFOUND: 'CORREO NO REGISTRADO',
    LOGIN_SUCCESSFULL: 'INGRESO CORRECTO',
    DATA_SUCCESSFULL: 'DATOS CORRECTOS',
    MAIL_SENT: 'ENVIO DE CORREO EXITOSO',
    MAIL_FAIL: 'OCURRIO UN PROBLEMA AL ENVIAR EL CORREO',
    USERDATA_FOUND: 'USUARIO REGISTRADO EN MODULO DOCENTE',
    USERDATA_NOTFOUND: 'USUARIO NO REGISTRADO EN MODULO DOCENTE',
    RESET_PASSWORD: 'CONTRASEÑA ACTUALIZADA CORRECTAMENTE',
    COURSE_NOTFOUND: 'NO SE ENCONTRARON CURSOS RELACIONADOS',
    RESET_PASSWORD_ERROR: 'OCURRIO UN PROBLEMA AL ACTUALIZAR CONTRASEÑA',
    RECOVER_PASSWORD: 'RECUPAR CONTRASEÑA CORREO ISIL',
    COMMISSION_TOTAL: 'COMISIONES TOTALES',
    COMMISSION_TOTAL_FILTER: 'COMISIONES TOTALES FILTRADAS',
    COMMISSION_NOTFOUND: 'SIN COMISIONES TOTALES',
}


export {
    TABLES,
    API,
    SP,
    STATUS,
    MESSAGE
}