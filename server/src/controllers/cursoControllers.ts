import { Request, Response } from 'express';
import { Prisma, PrismaClient } from '@prisma/client';
import { TABLES, STATUS, MESSAGE } from '../helpers/constants';
import { DefaultResponse } from '../model/defaultResponse';

const responseModel = new DefaultResponse();
const prisma = new PrismaClient();

export const getCoursesByTeacher = async(req:Request, res: Response) => {        
    res.setHeader('Content-Type', 'application/json');
    try {            
        res.status(200);
        const paramIdTeacher = req.query.docente_id;
        const result: any[] =  await prisma.$queryRawUnsafe(`SELECT  A.curso
                                                                ,B.nombre as categoria
                                                                ,A.costoFinal as costo
                                                                ,A.montoComision as comision
                                                                ,A.created_at
                                                        FROM ${TABLES.TABLE_COURSE_SELL} A
                                                        INNER JOIN ${TABLES.TABLE_COURSE} B
                                                            ON A.CURSO_ID = B.ID
                                                        INNER JOIN ${TABLES.TABLE_CATEGORY} C
                                                            ON B.CATEGORIA_ID = C.ID
                                                        WHERE A.DOCENTE_ID = ?`,
                              paramIdTeacher);
        if (result.length != 0) {
            responseModel.Status = STATUS.OK
            responseModel.Message = MESSAGE.DATA_SUCCESSFULL,
            responseModel.Data = result
        }
        else{
            res.status(404);
            responseModel.Status = STATUS.OK
            responseModel.Message = MESSAGE.COURSE_NOTFOUND,
            responseModel.Data = null
        }
    } catch (error) {
        res.status(500);
        responseModel.Status = STATUS.FAIL
        responseModel.Message = error.stack,
        responseModel.Data = null
    }
    res.send(JSON.stringify(responseModel));
}

export const  getListAllCourse = async (req: Request, res: Response)=>{
    const curso = await prisma.$queryRaw`select c.nombre as curso, ca.nombre as categoria from curso c left join categoria ca on ca.id = c.categoria_id`
    return res.status(200).json({
        curso: curso
    })
}
