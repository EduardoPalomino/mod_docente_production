import { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { TABLES, STATUS, MESSAGE } from '../helpers/constants';
import { DefaultResponse } from '../model/defaultResponse';

const prisma = new PrismaClient();
const responseModel = new DefaultResponse();

export const welcomeUser = async (req: Request, res: Response)=>{
    res.status(200);
    const data = { Mensaje: "Bienvenido"}
    res.send(JSON.stringify(data));
}

export const validateAuthUser = async (req: Request, res: Response)=>{
    res.setHeader('Content-Type', 'application/json');
    try {        
        res.status(200);
        const correctLogin = req.oidc.isAuthenticated() ? MESSAGE.LOGGED_IN : MESSAGE.LOGGED_OUT
    if (correctLogin === MESSAGE.LOGGED_IN) {
        responseModel.Status = STATUS.OK        
        const user =req.oidc.user;
        const resultLogin: any[] =  await prisma.$queryRawUnsafe(`SELECT * FROM ${TABLES.TABLE_USER} WHERE correo = ?`,
                                       user.email);
        if (resultLogin.length != 0) {
            responseModel.Status = STATUS.OK
            responseModel.Message = MESSAGE.USERDATA_FOUND,
            responseModel.Data = resultLogin;
        }else{
            res.status(404);
            responseModel.Status = STATUS.FAIL
            responseModel.Message = MESSAGE.USERDATA_NOTFOUND,
            responseModel.Data = null;
        }
    } else {
        res.status(200);
        responseModel.Status = STATUS.OK
        responseModel.Message = MESSAGE.LOGGED_OUT,
        responseModel.Data = null
    }
 } catch (error) {
    res.status(500);
    responseModel.Status = STATUS.FAIL
    responseModel.Message = error.stack,
    responseModel.Data = null
 }
 res.send(JSON.stringify(responseModel));
}