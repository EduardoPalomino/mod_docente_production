import { Request, Response } from 'express';
import { Prisma, PrismaClient } from '@prisma/client';
import { TABLES, STATUS, MESSAGE } from '../helpers/constants';
import { DefaultResponse } from '../model/defaultResponse';
import { sendMail } from '../helpers/mail';

const responseModel = new DefaultResponse();
const prisma = new PrismaClient();

export const  validateUser = async (req: Request, res: Response)=>{
    res.setHeader('Content-Type', 'application/json');    
    try {
        res.status(200);
        const mailTeacher = req.query.docente_mail;
        const resultLogin: any[] =  await prisma.$queryRawUnsafe(`SELECT * FROM ${TABLES.TABLE_USER} WHERE correo = ?`,
                                   mailTeacher);
    if (resultLogin.length != 0) {
        responseModel.Status = STATUS.OK
        responseModel.Message = MESSAGE.USERDATA_FOUND,
        responseModel.Data = resultLogin;
    }else{
        res.status(404);
        responseModel.Status = STATUS.FAIL
        responseModel.Message = MESSAGE.USERDATA_NOTFOUND,
        responseModel.Data = null;
    }
    } catch (error) {
        res.status(500);
        responseModel.Status = STATUS.FAIL
        responseModel.Message = error.stack,
        responseModel.Data = null   
    }
    res.send(JSON.stringify(responseModel));
}

    export const loginUser = async (req:Request, res: Response) => {
        res.setHeader('Content-Type', 'application/json');
        try {   
            res.status(200);
            const resultLogin: any[] =  await prisma.$queryRawUnsafe(`SELECT * FROM ${TABLES.TABLE_USER} WHERE correo = ?`,
                                       req.body.usuario);
            if (resultLogin.length == 0) {
                res.status(404);
                responseModel.Status = STATUS.OK
                responseModel.Message = MESSAGE.USER_NOTFOUND
                responseModel.Data = null
            }else{
                const result:any[] =  await prisma.$queryRawUnsafe(`SELECT * FROM ${TABLES.TABLE_USER} WHERE correo = ? and password = ?`,
                req.body.usuario,
                req.body.password);
                if (result.length != 0) {
                responseModel.Status = STATUS.OK
                responseModel.Message = MESSAGE.LOGIN_SUCCESSFULL,
                responseModel.Data = result
                }
                else{
                    res.status(404);
                    responseModel.Status = STATUS.OK
                    responseModel.Message = MESSAGE.PASSWORD_NOTFOUND,
                    responseModel.Data = null
                }
            }
        } catch (error) {
            res.status(500);
            responseModel.Status = STATUS.FAIL
            responseModel.Message = error.stack,
            responseModel.Data = null
        }
        res.send(JSON.stringify(responseModel));
    }

    export const recoverPassword = async (req:Request, res: Response) => {
        res.setHeader('Content-Type', 'application/json');
        try {
            res.status(200);
            const result: any[] =  await prisma.$queryRawUnsafe(`SELECT * FROM ${TABLES.TABLE_USER} WHERE correo = ?`,
                             req.body.usuario);
            if (result.length != 0) {
                // req.body.usuario ='galegriaq05@gmail.com';
                const resultSend = await sendMail(req.body.usuario);
                if(!resultSend){
                    responseModel.Status = STATUS.OK
                    responseModel.Message = MESSAGE.MAIL_SENT,
                    responseModel.Data = result
                }
                else{
                    res.status(500);
                    responseModel.Status = STATUS.FAIL
                    responseModel.Message = MESSAGE.MAIL_FAIL,
                    responseModel.Data = null
                }
            }
            else{
                res.status(404);
                responseModel.Status = STATUS.OK
                responseModel.Message = MESSAGE.USER_NOTFOUND,
                responseModel.Data = null
            }
        } catch (error) {
            res.status(500);
            responseModel.Status = STATUS.FAIL
            responseModel.Message = error.stack,
            responseModel.Data = null
        }
        res.send(JSON.stringify(responseModel));
    }

    export const resetPassword = async (req:Request, res: Response) => {
        res.setHeader('Content-Type', 'application/json');
        try {           
            res.status(200); 
            const result =  await prisma.$executeRawUnsafe(`UPDATE ${TABLES.TABLE_USER} SET password = ? WHERE correo = ?`,
                             req.body.password,
                             req.body.usuario);
            if (result == 1) {
                responseModel.Status = STATUS.OK
                responseModel.Message = MESSAGE.RESET_PASSWORD,
                responseModel.Data = [{"NEW_PASSWORD: ": req.body.password}]
            }
            else{
                res.status(500);
                responseModel.Status = STATUS.OK
                responseModel.Message = MESSAGE.RESET_PASSWORD_ERROR,
                responseModel.Data = null
            }
        } catch (error) {
            res.status(500);
            responseModel.Status = STATUS.FAIL
            responseModel.Message = error.stack,
            responseModel.Data = null
        }
        res.send(JSON.stringify(responseModel));
    }

    export const  postAllTeacherCourseStateCommissionRate = async (req: Request, res: Response)=>{
        const usuario = await prisma.$queryRaw`SELECT 	d.nombre,
                                                        d.paterno,
                                                        d.materno,
                                                        d.cargo as cargo,
                                                        es.nombre as estado,
                                                        CAST(count(dc.id) AS CHAR(50)) as nro_cursos,
                                                        CAST(SUM(c.costoFinal*(SELECT (PORCENTAJE/100) FROM comision)) as CHAR(50)) as total_comision
                                                FROM docente d
                                                LEFT JOIN estado es 
                                                    ON es.id = d.estado_id
                                                LEFT JOIN docente_curso dc 
                                                    ON dc.id = d.id
                                                LEFT JOIN curso c 
                                                    ON c.id = dc.curso_id
                                                GROUP BY d.nombre,
                                                         d.paterno,
                                                         d.materno,
                                                         d.cargo,
                                                         es.nombre,
                                                         c.costoFinal`
        return res.status(200).json({
            usuario: usuario
        })
    }
    export const  putTeacherInfo = async (req: Request, res: Response)=>{
        const nombre:string = req.body.nombre;
        const paterno:string = req.body.paterno;
        const materno:string = req.body.materno;
        const foto:string = req.body.foto;
        const correo:string = req.body.correo;
        const cargo:string = req.body.cargo;
        const estado_id:number = req.body.estado_id;
        const tipo_tasa_id:number = req.body.estado_id;
        const id:number = req.body.id;
        const usuario = await prisma.$queryRaw`UPDATE docente SET 
        nombre = nombre,
        paterno = paterno,
        materno = materno,
        foto = foto,
        correo = correo,
        cargo = cargo,
        estado_id = estado_id,
        tipo_tasa_id = tipo_tasa_id 
        WHERE docente.id = id`
        return res.status(200).json({
            usuario: usuario
        })
    }

    export const  getStateTeacher = async (req: Request, res: Response)=>{
        const estado = await prisma.$queryRaw`SELECT * from estado`
        return res.status(200).json({
            estado: estado
        })
    }

    export const  getListAllRateType = async (req: Request, res: Response)=>{
        const tipotasa = await prisma.$queryRaw`SELECT * from tipo_tasa`
        return res.status(200).json({
            tipotasa: tipotasa
        })}