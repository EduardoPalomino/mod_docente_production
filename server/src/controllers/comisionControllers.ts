import { Request, Response } from 'express';
import { Prisma, PrismaClient } from '@prisma/client';
import { TABLES, STATUS, MESSAGE } from '../helpers/constants';
import { DefaultResponse } from '../model/defaultResponse';
import moment from 'moment';

const responseModel = new DefaultResponse();
const prisma = new PrismaClient();

const getAllCommission = async (paramIdTeacher) =>{
    const result: any[]=  await prisma.$queryRawUnsafe(`SELECT  SUM(costoFinal*(SELECT (PORCENTAJE/100) FROM ${TABLES.TABLE_COMMISSION})) as comision 
                                                          FROM  ${TABLES.TABLE_COURSE_SELL} cv
                                                          WHERE cv.docente_id=?`,
                                paramIdTeacher);
    if (result.length == 0) return null;
    return result[0]
}

const getAllCommissionByActualMonth = async (paramIdTeacher) =>{
    const firstDateOfMonth = moment().startOf('month').format('YYYY-MM-DD');
    const lastDateOfMonth = moment().endOf('month').format('YYYY-MM-DD');
    const result: any[] =  await prisma.$queryRawUnsafe(`SELECT  SUM(costoFinal*(SELECT (PORCENTAJE/100) FROM ${TABLES.TABLE_COMMISSION})) as comision 
                                                          FROM  ${TABLES.TABLE_COURSE_SELL} cv
                                                          WHERE cv.docente_id=?
                                                          AND created_at BETWEEN ? AND ?`,
                                paramIdTeacher,
                                firstDateOfMonth,
                                lastDateOfMonth);
    if (result.length == 0) return null;
    return result[0]
}

const getRecentCommissions = async (paramIdTeacher) =>{
    const result: any[]= await prisma.$queryRawUnsafe(`SELECT * FROM ${TABLES.TABLE_COURSE_SELL}
                                                      WHERE docente_id=?
                                                      ORDER BY CREATED_AT DESC`,
                                paramIdTeacher);
    if(result.length == 0) return null;    
    return result;
}

const getCursesSell = async (paramIdTeacher) =>{
    const result: any[] =  await prisma.$queryRawUnsafe(`SELECT CAST(COUNT(*) as CHAR(50)) as total_cursos,
                                                             CAST(COUNT(CASE WHEN FLAG= 1 THEN 1 ELSE NULL END) as CHAR(50)) as total_cursos_activos 
                                                      FROM ${TABLES.TABLE_COURSE_SELL}
                                                      WHERE DOCENTE_ID = ?`,
                                                      paramIdTeacher);
    if (result.length == 0) return null;
    return result[0]
}

export const getTotalCommission = async(req:Request, res: Response) => {
    res.setHeader('Content-Type', 'application/json');    
    try {
        const paramIdTeacher = req.query.docente_id;
        res.status(200);
        const resultAllCommission = await getAllCommission(paramIdTeacher);
        if (resultAllCommission !== null) {
            responseModel.Status = STATUS.OK
            responseModel.Message = MESSAGE.COMMISSION_TOTAL
            responseModel.Data = resultAllCommission
        }else{
            res.status(404);
            responseModel.Status = STATUS.OK
            responseModel.Message = MESSAGE.COMMISSION_NOTFOUND,
            responseModel.Data = null
        }
    } catch (error) {
        res.status(500);
        responseModel.Status = STATUS.FAIL
        responseModel.Message = error.stack,
        responseModel.Data = null
    }
    res.send(JSON.stringify(responseModel));
}

export const getTotalCommissionFilter = async(req:Request, res: Response) => {
    res.setHeader('Content-Type', 'application/json');    
    try {
        const paramIdTeacher = req.query.docente_id;
        res.status(200);
        const category = req.body.cursoCategoria.toUpperCase();
        const result: any[] =  await prisma.$queryRawUnsafe(`SELECT  A.curso
                                                                    ,B.nombre as categoria
                                                                    ,A.costoFinal as costo
                                                                    ,A.montoComision as comision
                                                                    ,A.created_at
                                                            FROM ${TABLES.TABLE_COURSE_SELL} A
                                                            INNER JOIN ${TABLES.TABLE_COURSE} B
                                                                ON A.CURSO_ID = B.ID
                                                            INNER JOIN ${TABLES.TABLE_CATEGORY} C
                                                                ON B.CATEGORIA_ID = C.ID
                                                            WHERE A.DOCENTE_ID = ?
                                                            AND UPPER(c.nombre) like '%${category}%'`,
                                paramIdTeacher);                                
        if (result.length !== 0) {
            responseModel.Status = STATUS.OK
            responseModel.Message = MESSAGE.COMMISSION_TOTAL_FILTER
            responseModel.Data = result
        }else{
            res.status(404);
            responseModel.Status = STATUS.OK
            responseModel.Message = MESSAGE.COMMISSION_NOTFOUND,
            responseModel.Data = null
        }
    } catch (error) {
        res.status(500);
        responseModel.Status = STATUS.FAIL
        responseModel.Message = error.stack,
        responseModel.Data = null
    }
    res.send(JSON.stringify(responseModel));
}

export const getGeneralDetail = async(req:Request, res: Response) => {
    res.setHeader('Content-Type', 'application/json');
    try {
        res.status(200);          
        const generalDetail= {
            ComisionesTotales: {},
            ComisiionesMesActual: {},
            ComisionesRecientes: [],
            CursosVendidos: {},
        }
        const paramIdTeacher = req.query.docente_id;
        const resultAllCommission = await getAllCommission(paramIdTeacher);
        const resultCommissionByMonth = await getAllCommissionByActualMonth(paramIdTeacher);
        const resultRecentCommsion = await getRecentCommissions(paramIdTeacher);
        const resultCourseSell = await getCursesSell(paramIdTeacher);
        generalDetail.ComisionesTotales = resultAllCommission;
        generalDetail.ComisiionesMesActual = resultCommissionByMonth;
        generalDetail.ComisionesRecientes = resultRecentCommsion;
        generalDetail.CursosVendidos = resultCourseSell;

        responseModel.Status = STATUS.OK;
        responseModel.Message = MESSAGE.DATA_SUCCESSFULL;
        responseModel.Data = [];
        responseModel.Data.push(generalDetail);
    } catch (error) {
        res.status(500);
        responseModel.Status = STATUS.FAIL
        responseModel.Message = error.stack,
        responseModel.Data = null
    }
    res.send(JSON.stringify(responseModel));
}

export const  postAllUserCommission = async (req: Request, res: Response)=>{
    const comision = await prisma.$queryRaw`SELECT cv.created_at as FechaVenta, c.nombre as curso ,ca.nombre as categoria ,c.costoFinal as costo_curso,c.costoFinal*(SELECT (PORCENTAJE/100) FROM comision) as comision 
    FROM  curso_vendido cv
    LEFT JOIN curso c ON c.id = cv.curso_id 
    LEFT JOIN categoria ca ON  ca.id = c.categoria_id`
    return res.status(200).json({
        comision: comision
    })
}


export const  postAllCourseComision = async (req: Request, res: Response)=>{
    const comision = await prisma.$queryRaw`SELECT cv.docente_id, c.created_at as FechaCreacion, c.nombre as curso,c.descripcion,ca.nombre as categoria ,
    SUM(c.costoFinal) as costo_curso,
    SUM(c.costoFinal*(SELECT (PORCENTAJE/100) FROM comision)) as comision 
    FROM  curso_vendido cv
    LEFT JOIN curso c ON c.id = cv.curso_id 
    LEFT JOIN categoria ca ON  ca.id = c.categoria_id
    WHERE cv.docente_id=1
    GROUP BY cv.docente_id,c.created_at,c.nombre,ca.nombre,c.descripcion`
    return res.status(200).json({
        comision: comision
    })
}
export const  updateCommissionCourseMassive = async (req: Request, res: Response)=>{
    const id: Number = req.body.id;
    const tasacomision: Number = req.body.tasacomision;
    const comision = await prisma.$queryRaw`UPDATE docente_curso SET tasacomision = 22 WHERE docente_curso.id = id`
    return res.status(200).json({
        comision: comision
    })
}
export const  updateCommissionTeacherMassive = async (req: Request, res: Response)=>{
    const id: Number = req.body.id;
    const tasacomision: Number = req.body.tasacomision;
    const comision = await prisma.$queryRaw`UPDATE docente_curso SET tasacomision = 22 WHERE docente_id = id`
    return res.status(200).json({
        comision: comision
    })
}