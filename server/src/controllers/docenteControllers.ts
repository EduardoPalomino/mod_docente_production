import { Request, Response } from 'express';
import { DefaultResponse } from '../model/defaultResponse';
import { PrismaClient } from '@prisma/client';
import { TABLES, STATUS, MESSAGE } from '../helpers/constants';

const responseModel = new DefaultResponse();
const prisma = new PrismaClient();

export const getTeacherInfo = async(req:Request, res: Response) => {        
    res.setHeader('Content-Type', 'application/json');
    try {         
        res.status(200);   
        const paramUserLogin = req.query.docente_id;
        const result: any[] =  await prisma.$queryRawUnsafe(`SELECT * FROM ${TABLES.TABLE_TEACHER} WHERE id = ?`,
                              paramUserLogin);
        if (result.length != 0) {
            responseModel.Status = STATUS.OK
            responseModel.Message = MESSAGE.DATA_SUCCESSFULL,
            responseModel.Data = result
        }
        else{
            res.status(404);
            responseModel.Status = STATUS.OK
            responseModel.Message = MESSAGE.USERDATA_NOTFOUND,
            responseModel.Data = null
        }
    } catch (error) {
        res.status(500);
        responseModel.Status = STATUS.FAIL
        responseModel.Message = error.stack,
        responseModel.Data = null
    }
    res.send(JSON.stringify(responseModel));
}

export const  postAllTeacherCommissionCourse = async (req: Request, res: Response)=>{
    const docente = await prisma.$queryRaw`
    SELECT d.id,d.nombre,d.paterno,d.materno,d.foto,d.correo,r.nombre as rol,c.nombre as cargo FROM docente d
     LEFT JOIN rol r ON  r.id = d.rol_id
     LEFT JOIN cargo c ON  c.id = d.cargo_id
     `
    return res.status(200).json({
        docente: docente
    })
}
export const  getListAllTeacher = async (req: Request, res: Response)=>{
    const docente = await prisma.$queryRaw`select * from docente`
    return res.status(200).json({
        docente: docente
    })
}
