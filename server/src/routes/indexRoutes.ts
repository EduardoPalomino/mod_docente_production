import {Router} from 'express';
import * as usuarioControllers from '../controllers/usuarioControllers';
import * as categoryControllers from '../controllers/categoriaControllers';
import * as rolControllers from '../controllers/rolControllers';
import * as cursoControllers from '../controllers/cursoControllers';
import * as comisionControllers from '../controllers/comisionControllers';
import { API } from '../helpers/constants';
import * as validacionController from '../controllers/validacionController';
import { requiresAuth } from'express-openid-connect';
import  * as docenteControllers from '../controllers/docenteControllers';


class IndexRoutes{
  public router : Router = Router();

  constructor(){
   this.config();
  }

  config():void{
    this.router.get(API.VALIDATION,validacionController.welcomeUser);
    //  this.router.get(API.VALIDATION,requiresAuth(), validacionController.validateAuthUser) -- EXAMPLE
    //USUARIO
    this.router.get(API.VALIDATE_USER,usuarioControllers.validateUser);    
    this.router.get(API.GET_STATE_TEACHER,usuarioControllers.getStateTeacher);  
    this.router.get(API.GET_LIST_ALL_RATE_TYPE,usuarioControllers.getListAllRateType);  
    //COMISION
    this.router.get(API.GET_GENERAL_DETAIL, comisionControllers.getGeneralDetail)
    this.router.get(API.GET_TOTAL_COMMISSION, comisionControllers.getTotalCommission)
    this.router.post(API.GET_TOTAL_COMMISSION_FILTER, comisionControllers.getTotalCommissionFilter)
    this.router.post(API.POST_ALL_USER_COMMISSION,comisionControllers.postAllUserCommission);   
    this.router.post(API.POST_ALL_COURSE_COMMISSION,comisionControllers.postAllCourseComision);   
    this.router.put(API.UPDATE_COMMISSION_COURSE_MASSIVE,comisionControllers.updateCommissionCourseMassive);   
    this.router.put(API.UPDATE_COMMISSION_TEACHER_MASSIVE,comisionControllers.updateCommissionTeacherMassive);
    //DOCENTE
    this.router.get(API.GET_TEACHER_INFO, docenteControllers.getTeacherInfo);
    this.router.post(API.POST_ALL_TEACHER_COMMISSION_COURSE,docenteControllers.postAllTeacherCommissionCourse);
    this.router.post(API.GET_LIST_ALL_TEACHER,docenteControllers.getListAllTeacher)
    this.router.post(API.POST_ALL_TEACHER_COURSE_STATE_COMMISSION_RATE,usuarioControllers.postAllTeacherCourseStateCommissionRate);
    this.router.put(API.PUT_TEACHER_INFO,usuarioControllers.putTeacherInfo);  
    //CURSO
    this.router.get(API.GET_COURSES_BY_TEACHER, cursoControllers.getCoursesByTeacher);
    this.router.post(API.GET_LIST_ALL_COURSE,cursoControllers.getListAllCourse);  
  }
}

const indexRoutes = new IndexRoutes();
export default indexRoutes.router; 