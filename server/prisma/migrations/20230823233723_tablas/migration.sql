-- CreateTable
CREATE TABLE `Rol` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(30) NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Cargo` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(30) NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Estado` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(30) NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Tipo_Tasa` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(30) NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Usuario` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(30) NOT NULL,
    `paterno` VARCHAR(30) NOT NULL,
    `materno` VARCHAR(30) NOT NULL,
    `foto` VARCHAR(30) NOT NULL,
    `correo` VARCHAR(191) NOT NULL,
    `password` VARCHAR(30) NOT NULL,
    `cargo` VARCHAR(191) NOT NULL,
    `rol_id` INTEGER NOT NULL,
    `estado_id` INTEGER NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    UNIQUE INDEX `Usuario_correo_key`(`correo`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Docente` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(30) NOT NULL,
    `paterno` VARCHAR(30) NOT NULL,
    `materno` VARCHAR(30) NOT NULL,
    `foto` VARCHAR(30) NOT NULL,
    `correo` VARCHAR(191) NOT NULL,
    `password` VARCHAR(191) NOT NULL,
    `cargo` VARCHAR(191) NOT NULL,
    `rol_id` INTEGER NOT NULL,
    `estado_id` INTEGER NOT NULL,
    `tipo_tasa_id` INTEGER NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    UNIQUE INDEX `Docente_correo_key`(`correo`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Comision` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `porcentaje` VARCHAR(30) NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Membresia` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `tipo` VARCHAR(30) NOT NULL,
    `descripcion` VARCHAR(191) NOT NULL,
    `porcentaje` VARCHAR(10) NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Categoria` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(30) NOT NULL,
    `descripcion` VARCHAR(191) NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Curso` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `codigo` VARCHAR(20) NOT NULL,
    `nombre` VARCHAR(30) NOT NULL,
    `descripion` VARCHAR(191) NOT NULL,
    `categoria_id` INTEGER NOT NULL,
    `costoFijo` VARCHAR(20) NOT NULL,
    `costoOferta` VARCHAR(20) NOT NULL,
    `costoFinal` VARCHAR(20) NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Docente_curso` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `docente_id` INTEGER NOT NULL,
    `curso_id` INTEGER NOT NULL,
    `comision` INTEGER NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Curso_vendido` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `curso_id` INTEGER NOT NULL,
    `curso` VARCHAR(50) NOT NULL,
    `docente_id` INTEGER NOT NULL,
    `costoFinal` VARCHAR(20) NOT NULL,
    `montoComision` VARCHAR(20) NOT NULL,
    `flag` INTEGER NOT NULL DEFAULT 0,
    `created_at` DATETIME NOT NULL,
    `updated_at` TIMESTAMP NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
